#lang racket

(require pict
         racket/draw)

(define fg-color-foss
  (make-color #xEE #xEE #xEC))

(define bg-color-foss
  (make-color #x2E #x2B #x2B))

(define bg-color-crafts
  (make-color #xEE #xEE #xEE))

(define fg-color-crafts
  (make-color #x67 #x21 #x15))

(define text-logo1
  "\
.------------.
|            |
|    FOSS    |
|-----&&-----|
|   CRAFTS   |
|            |
'------------'")

#;(define text-logo1
  "\
.------------.
|            |
|    FOSS    |
|            |
|-----&&-----|
|            |
|   CRAFTS   |
|            |
'------------'")

#;(define text-logo1
  "\
.------------.
|            |
|    FOSS    |
|            |
|     &&     |
|            |
|   CRAFTS   |
|            |
'------------'")

(define text-logo3
  "\
.------------.
|            |
|    FOSS    |
|       _____|
|_____&&     |
|            |
|   CRAFTS   |
|            |
'------------'")

(define text-logo2
  "\
.--------.
|  FOSS  |
|---&&---|
| CRAFTS |
'--------'")

(define text-logo-top
  "\
.------------.
|            |
|    FOSS    |
|       _____|")


;; .------------.
;; |            |
;; |    FOSS    |
;; |            |
;; |-----&&-----|
;; |            |
;; |   CRAFTS   |
;; |            |
;; '------------'
;; 
;; 
;; .--------.
;; |  FOSS  |
;; |---&&---|
;; | CRAFTS |
;; '--------'


(define text-lines
  (string-split text-logo3 "\n"))

(define line-count
  (length text-lines))

(define halfish-lines
  (floor (/ line-count 2) ))

(define (make-logo-wip)
  (let* ([logo-fg-foss
          (apply vc-append
                 (for/list ([line (string-split text-logo1 "\n")])
                   (text line (cons fg-color-foss "VT323") 36)))]
         [logo-fg-crafts
          (apply vc-append
                 (for/list ([line (string-split text-logo1 "\n")])
                   (text line (cons fg-color-crafts "VT323") 36)))]
         [width (pict-width logo-fg-foss)]
         [height (pict-height logo-fg-foss)]
         [square-size (* (max width height) 1.05)]
         [halfsquare (/ square-size 2)]
         [logo-foss
          (cc-superimpose (filled-rectangle square-size square-size
                                            #:color bg-color-foss)
                          logo-fg-foss)]
         [logo-crafts
          (cc-superimpose (filled-rectangle square-size square-size
                                            #:color bg-color-crafts)
                          logo-fg-crafts)])
    (vc-append
     (inset/clip logo-foss 0 0 0 (* halfsquare -1))
     (inset/clip logo-crafts 0 (* halfsquare -1) 0 0))))


(define (text-lines->raart lines fg-color bg-color
                           #:font-size [font-size 36])
  (define lines-fg
    (apply vc-append
           (for/list ([line lines])
             (text line (cons fg-color "VT323") font-size))))

  (cc-superimpose (filled-rectangle (pict-width lines-fg)
                                    (pict-height lines-fg)
                                    #:color bg-color
                                    #:draw-border? #f)
                  lines-fg))

(define (make-logo [font-size 42])
  (define foss-top
    (text-lines->raart '(".------------."
                         "|            |"
                         "|    FOSS    |")
                       fg-color-foss bg-color-foss
                       #:font-size font-size))
  (define crafts-bottom
    (text-lines->raart '("|   CRAFTS   |"
                         "|            |"
                         "'------------'")
                       fg-color-crafts bg-color-crafts
                       #:font-size font-size))
  (define middle-left
    (text-lines->raart '("|     ")
                       fg-color-foss bg-color-foss
                       #:font-size font-size))
  (define middle-right
    (text-lines->raart '("     |")
                       fg-color-crafts bg-color-crafts
                       #:font-size font-size))

  (define foss-&-fg
    (text "&" (cons fg-color-foss "VT323") font-size))
  (define crafts-&-fg
    (text "&" (cons fg-color-crafts "VT323") font-size))
  (define foss-&-bg
    (cc-superimpose
     (filled-rectangle (pict-width foss-&-fg)
                       (pict-height foss-&-fg)
                       #:color bg-color-crafts
                       #:draw-border? #f)
     (inset/clip (filled-rounded-rectangle (* (pict-width foss-&-fg) 2)
                                           (* (pict-height foss-&-fg) 2)
                                           #:color bg-color-foss
                                           #:draw-border? #f)
                 (* (pict-width foss-&-fg) -1)
                 (* (pict-height foss-&-fg) -1)
                 0 0)))
  (define crafts-&-bg
    (cc-superimpose
     (filled-rectangle (pict-width crafts-&-fg)
                       (pict-height crafts-&-fg)
                       #:color bg-color-foss
                       #:draw-border? #f)
     (inset/clip (filled-rounded-rectangle (* (pict-width crafts-&-fg) 2)
                                           (* (pict-height crafts-&-fg) 2)
                                           #:color bg-color-crafts
                                           #:draw-border? #f)
                 0 0
                 (* (pict-width crafts-&-fg) -1)
                 (* (pict-height crafts-&-fg) -1))))
  (define foss-&
    (cc-superimpose foss-&-bg foss-&-fg))
  (define crafts-&
    (cc-superimpose crafts-&-bg crafts-&-fg))
  
  (vc-append foss-top
             (hc-append middle-left foss-& crafts-& middle-right)
             crafts-bottom))
