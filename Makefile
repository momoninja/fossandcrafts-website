compile_and_run: html serve

html:
	guix environment -l guix.scm -- haunt build

serve:
	guix environment -l guix.scm -- haunt serve --watch




