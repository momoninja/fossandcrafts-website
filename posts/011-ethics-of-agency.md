title: 11: An Ethics of Agency
date: 2020-10-01 11:59
tags: episode, philosophy
slug: 11-an-ethics-of-agency
summary: An exploration of a philosophical framework Chris has been workshopping for a few years, "An Ethics of Agency"
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/011-ethics-of-agency.ogg" length:"49076770" duration:"00:59:25"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/011-ethics-of-agency.mp3" length:"57049284" duration:"00:59:25"
---
Chris and Morgan discuss an ethical framework Chris has been
workshopping for the last few years, "An Ethics of Agency", with the
foundation of maximizing agency "for you, for me, for everyone" and
minimizing subjection.
**CW:** *Note that Chris talks about an incident involving them experiencing suicidal depression at one point.*

**Links:**

 - Other philosophical systems mentioned:

   - [Utilitarianism](https://en.wikipedia.org/wiki/Utilitarianism)

   - [Kantianism](https://en.wikipedia.org/wiki/Kantianism)

   - [Ethics of Care](https://en.wikipedia.org/wiki/Ethics_of_care)

 - [Peter Singer](https://en.wikipedia.org/wiki/Peter_Singer)'s book
   [Animal Liberation](https://en.wikipedia.org/wiki/Animal_Liberation_(book)),
   and the argument for
   [Equal consideration of interests](https://en.wikipedia.org/wiki/Equal_consideration_of_interests).
   (Note that Peter Singer gets criticism from some disability circles;
   this is a [good summary](https://aeon.co/ideas/what-i-learned-about-disability-and-infanticide-from-peter-singer).
   In general it's our position to focus on "raising up" rights,
   including those of animals; pitting animal rights vs disabled rights need
   not be done in a society with as many resources as ours presently is.)

 - [Amartya Sen](https://en.wikipedia.org/wiki/Amartya_Sen), whose book
   [Development as Freedom](https://en.wikipedia.org/wiki/Development_as_Freedom)
   had a bigger background influence than Chris probably realized in
   its treatment of the agency of people as the primary index by which
   we measure a country's development

 - [The GNU Manifesto](https://www.gnu.org/gnu/manifesto.en.html).
   Search for "Kantian ethics" on the page.
   (Curiously its preceding sentence is described in
   an example that appears consequentialist!  By the way, pretty much
   every decent ethical system claims that its foundation is the
   "golden rule", this isn't unique to Kantianism.)

 - [The Free Software Definition](https://www.gnu.org/philosophy/free-sw.en.html).
   Also note the pun on another speech called
   [The Four Freedoms](https://en.wikipedia.org/wiki/Four_Freedoms).

 - [Free as in Freedom episode with the AGPL panel discussion](http://faif.us/cast/2013/jul/17/0x3F/)

 - A FOSDEM talk in 2014, [The Road Ahead for Network Freedom](https://archive.fosdem.org/2014/schedule/event/network_freedom/),
   where "freedom for developers, but not for users" is mentioned as a
   phrase

 - [Libre Lounge](https://librelounge.org/)'s subtitle: "a casual
   podcast about user freedom", including
   [mentioned episode with Karen Sandler](https://librelounge.org/episodes/episode-5-karen-sandler-and-software-freedom-conservancy.html)

 - Some talks in 2018 by Molly DeBlanc (and Karen Sandler) using the term
   "user freedom":

   - [That's a free software issue!](https://www.youtube.com/watch?v=qo_WH1bYgbo)
   
   - [User freedom: A love story](https://vimeo.com/305038607) 
   
 - Molly DeBlanc has a [wonderful article giving a personal definition of "user freedom"](http://deblanc.net/blog/2018/12/22/user-freedom-n/)

 - [Declaration of Digital Autonomy](https://techautonomy.org/)

 - [OCap conference 2018](https://2018.splashcon.org/track/ocap-2018-papers),
   source of the mentioned dinner between Chris, Mark Miller, Kate Sills
   
 - Chris's ActivityPub Conference 2019 keynote,
   [ActivityPub: past, present, future](https://conf.tube/videos/watch/2b9a985b-ccdd-49ce-a81b-ed00d2b47c85)
