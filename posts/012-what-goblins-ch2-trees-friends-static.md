title: 12: [Theatre] The What Goblins Saga, Chapter 2: Trees, Friends, and Static
date: 2020-10-08 16:00
tags: episode, theatre, rpg, freeform universal, what goblins
slug: 12-what-goblins-ch2-trees-friends-static
summary: The What Goblins saga continues as the characters continue to learn about themselves and their ever-changing environment
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/012-what-goblins-ch2-trees-friends-static.ogg" length:"64825246" duration:"01:40:10"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/012-what-goblins-ch2-trees-friends-static.mp3" length:"96164871" duration:"01:40:10"
---
On this episode of FOSS and Crafts Theatre, we continue the
What Goblins Saga.
The What Goblins saga continues as the characters continue to learn
about themselves and their ever-changing environment.
If you haven't listened to
[Chapter 1](https://fossandcrafts.org/episodes/10-what-goblins-ch1-what-are-goblins.html),
maybe stop reading now to avoid spoilers from that episode!

Having discovered that they are are sapient beings emergent from a
networked video game, and having accidentally stumbled into
administrative powers, the What Goblins discover the consequences of
using those powers without knowing how the world around them might
react to that.

**Links:**

 - [FOSS & Crafts Episode 1: Collabortive Storytelling with Dice](https://fossandcrafts.org/episodes/1-collaborative-storytelling-with-dice.html)
   introduces the idea of RPGs as a way of making narratives together.

 - See also [Freeform Universal](http://freeformuniversal.com/) (the
   RPG system used for this episode, explained in depth in Episode 1)!

 - And of course, see
   [Chapter 1 of the What Goblins Saga](https://fossandcrafts.org/episodes/10-what-goblins-ch1-what-are-goblins.html)!
